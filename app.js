const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const taskRoutes = require('./routes/taskRoutes.js')

// initialize dotenv usage
dotenv.config()

// express setup
const app = express()
const port = 3001

app.use(express.json()) // middleware
app.use(express.urlencoded({extended: true}))

// mongoose setup using S36-Discussion DB
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@zuitt-bootcamp.tveytx1.mongodb.net/S36-Discussion?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

let db = mongoose.connection 

db.on('error', () => console.error('Connection Error :('))
db.on('open', () => console.log('Connected to MongoDB!'))

// Routes
app.use('/tasks', taskRoutes)
// Make sure to assign a specific endpoint for every database collection. In this case, since we are manipulating the tasks collection in MongoDB. then we are specifying an endpoint with the specific routes and controllers for that collection.

// listen to port
app.listen(port,() => console.log(`Server is running at port ${port}`))



