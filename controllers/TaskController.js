const Task = require('../models/Task.js')

// 2. controller function for retrieving a specific task
module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then((task, error) => {
		if(error){
			return error
		}

		return task
	})
}

// 4. controller function for changing the status of a task to complete
module.exports.completeTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			return error
		}

		result.status = "complete"

		return result.save().then((completeTask, error) => {
			if(error){
				return error
			}

			return completeTask
		})
	})
}


// added to check mock data
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result 
	})
}

// creates a tasks for mock data
module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((savedTask, error) => {
		if(error){
			return error 
		}

		return 'Task created successfully!'
	})
}


