const express = require('express')
const TaskController = require('../controllers/TaskController.js')
const router = express.Router()

// 1. route for getting a specific task
router.get('/:id', (request, response) => {
	TaskController.getTask(request.params.id).then((task) => response.send(task))
})

// 2. route for changing the status of a task to complete
router.put('/:id/complete', (request, response) => {
	
	TaskController.completeTask(request.params.id, request.body).then((completeTask) =>
		response.send(completeTask))
})



// added for checking
router.get('/', (request, response) => {
	TaskController.getAllTasks().then((tasks) => response.send(tasks))
})

// added for mock data
router.post('/create', (request, response) => {
	TaskController.createTask(request.body).then((task) => response.send(task))
})





module.exports = router

